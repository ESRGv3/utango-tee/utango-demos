/**
 * SPDX-License-Identifier: Apache-2.0
 * Copyright (c) uTango Project and Contributors. All rights reserved.
 */

#include <utango.h>

#ifndef UTANGO_SIZE
/**
 * Declare WORLDS images using the WORLD_IMAGE macro, passing an identifier and 
 * the path for the image.
 */
WORLD_IMAGE(world1, ../../../worlds/world1/blinking_led.bin);
WORLD_IMAGE(world2, ../../../worlds/world2/cmdline.bin);
#endif

/**
 * Config each WORLD.
 */
world_cfg_t  worlds_cfg[UTANGO_WORLDS] =
{
    /* World 1 (Blinking Red LED) */
    {
        .id = 0,
        .vect_base_addr = 0x0a400000,
        .init_sp = 0x20004000,
        .entry_point = 0x0a400051,
        .n_regions = 4,
        .w_regions = (world_regions_t[])
        {
            {   /* FLASH (Code_SRAM2)*/
                .rgn_base_addr = 0x0a400000,
                .rgn_size = 0x8000
            },
            {   /* SRAM */
                .rgn_base_addr = 0x20000000,
                .rgn_size = 0x4000
            },
            {   /* PWM0 */
                .rgn_base_addr = 0x40101000,
                .rgn_size = 0x100
            },
            {   /* CMSDK Timer0 */
                .rgn_base_addr = 0x40000000,
                .rgn_size = 0x100
            }
        },
        .n_irqs = 1,
        .enabled_irqs = (irqs_list_t[])
        {CMSDK_TIMER0_IRQn},
    },
    /* World 2 (Serial Cmdline) */
    {
        .id = 1,
        .vect_base_addr = 0x0A420000,
        .init_sp = 0x20024000,
        .entry_point = 0x0a420145,
        .n_regions = 3,
        .w_regions = (world_regions_t[])
        {
            {   /* FLASH (Code_SRAM3)*/
                .rgn_base_addr = 0x0A420000,
                .rgn_size = 0x8000
            },
            {   /* RAM (SRAM3) */
                .rgn_base_addr = 0x20020000,
                .rgn_size = 0x4000
            },
            {   /* UART1 */
                .rgn_base_addr = 0x40106000,
                .rgn_size = 0x1000
            }
        },
        .n_irqs = 0,
        .enabled_irqs = (irqs_list_t[])
        {},
    },
};
