/**
 * SPDX-License-Identifier: Apache-2.0
 * Copyright (c) uTango Project and Contributors. All rights reserved.
 */
#include <pwm_ip6512.h>
#include <cmsdk_timers.h>

void cmsdktimer0_handler(void) __attribute__ ((interrupt ("irq")));
void pwm_init(void);
void pwm_high(void);
void pwm_low(void);
void cmsdktimer0_init(void);

void cmsdktimer0_handler(void){
    static bool led_is_on = false;
 
    if (led_is_on)
    {
        pwm_low();
    }
    else
    {
        pwm_high();
    }
    
    CMSDKTIMER0->INTSTATUSCLEAR |= CMSDKTIMER0_INTSTATUSCLEAR_INTCLEAR; 
    led_is_on = !led_is_on;
}

void cmsdktimer0_init(void)
{
    CMSDKTIMER0->RELOAD = 0x2625A00;

    CMSDKTIMER0->CTRL |= (CMSDKTIMER0_CTRL_EN | CMSDKTIMER0_CTRL_INTEN);
}

void pwm_init(void)
{
    /* Set PWM output continuously high (LED off) */
    PWM0->HR = 0x26259FF;

    /* Set PWM period to 1s (F=40MHz)
     * -> maximum value, output low, LED on
     * -> minimum value, output high, LED off
     * */
    PWM0->PR = 0x2625A00;

    /* Enable PWM interrupt */
    //PWM0->EI |= PWM_EI_EN;

    /* Enable PWM */
    PWM0->CR |= PWM_CR_EN;
    __asm__ volatile ("dsb");
}

void pwm_high(void)
{
    PWM0->CR &= ~PWM_CR_EN;
    PWM0->PR = 0xffffffff;
    PWM0->CR |= PWM_CR_EN;
}

void pwm_low(void)
{
    PWM0->CR &= ~PWM_CR_EN;
    PWM0->PR = 0x0;
    PWM0->CR |= PWM_CR_EN;
}

int main(void)
{
    uint32_t CMSDK_TIMER0_IRQn = 3;
    uint32_t *ptr= (uint32_t*)0xe000e100;

    *ptr = (uint32_t)(1UL << (CMSDK_TIMER0_IRQn & 0x1FUL));

    pwm_init();
    cmsdktimer0_init();
    
    while (1);
    /*{
        if(led_is_on)
        {
            pwm_low();
            led_is_on = false;
        }
        else
        {
            pwm_high();
            led_is_on = true;
        }
        
        for(int i = 0; i < 100000; i++);
    }*/

    return 0;
}
