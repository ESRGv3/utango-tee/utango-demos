/**
 * SPDX-License-Identifier: Apache-2.0
 * Copyright (c) uTango Project and Contributors. All rights reserved.
 */

#include "uart_pl011.h"
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/stat.h>

extern struct uart_pl011_dev_t UART0_PL011_DEV_NS;

int _read(int file, char *ptr, size_t len)
{
    int n_chars = 0;
    uint8_t byte = 0;

    if(ptr != NULL)
    {
        for(n_chars = 0; n_chars < len; ++ptr)
        {
            while (!uart_pl011_is_readable(&UART0_PL011_DEV_NS)){};
            uart_pl011_read(&UART0_PL011_DEV_NS, &byte);
            *ptr = (char)byte;
            ++n_chars;

            if((*ptr == '\n') || (*ptr == '\r'))
            {
                break;
            }
        }
    }
    return (n_chars);
}

size_t _write(int file, const void *ptr, size_t len) 
{
    if (isatty(file)) {

        const uint8_t * buff = (uint8_t *)ptr;

        for (size_t i = 0; i < len; i++) {

            while(!uart_pl011_is_writable(&UART0_PL011_DEV_NS)) {};

            /* As UART is ready to transmit at this point, the write function can not return any transmit error */
            (void)uart_pl011_write(&UART0_PL011_DEV_NS, buff[i]);

            if (buff[i] == '\n') {
                while(!uart_pl011_is_writable(&UART0_PL011_DEV_NS)) {};
                (void)uart_pl011_write(&UART0_PL011_DEV_NS, (uint8_t)'\r');
            }
        }

        return len;

    }

    return -1;
}

int _isatty(int file) 
{
    return (file == STDIN_FILENO || file == STDOUT_FILENO || file == STDERR_FILENO) ? 1 : 0;
}

void* _sbrk(int incr) 
{
    extern char _end[];
    extern char _heap_end[];
    static char *_heap_ptr = _end;

    if ((_heap_ptr + incr < _end) || (_heap_ptr + incr > _heap_end))
        return  (void *) -1;

    _heap_ptr += incr;
    
    return _heap_ptr - incr;
}

int _close(int file)
{
    return -1;
}

int _fstat(int file, struct stat *st) 
{
    st->st_mode = S_IFCHR;

    return 0;
}

int _lseek(int file, int ptr, int dir)
{
    return 0;
}

int open(const char *name, int flags, int mode)
{
    return -1;
}
