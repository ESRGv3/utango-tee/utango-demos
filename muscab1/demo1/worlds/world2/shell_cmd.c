#include "shell_cmd.h"

/**
 * @brief Shell help command
 */
void shell_help_cmd(char argc, char *argv)
{
    printf("Commands:\n");
    printf("-------------------------------------------------------\n");
    printf("h, help\t\t\t\tDisplay this help\n");
    printf("rst, reset\t\t\tForce a reset in W3\n");
    printf("w32, write \tADDR DATA\tWrite 32-bit word to memory \n");
    printf("r32, read \tADDR\t\tRead 32-bit word from memory \n");
}

/**
 * @brief Shell write command
 */
void shell_write_cmd(char argc, char *argv)
{
    if(argc < 4)
    {
        if(argv[argv[1]] != '\n' && argv[argv[2]] != '\n')
        {
            uint32_t const addr = (uint32_t)strtoul(&argv[argv[1]], NULL, 16);
            uint32_t const val = (uint32_t)strtoul(&argv[argv[2]], NULL, 16);

            __asm volatile("str %0, [%1]" : : "r"(val), "r"(addr));

            printf("Writing 0x%d to 0x%d address...\n");
            return;
        }
    }

    printf("ERROR: syntax wrong.\n");
    printf("Usage: w32, write \tADDR DATA\n");

    return;
}

/**
 * @brief Shell read command
 */
void shell_read_cmd(char argc, char *argv)
{
    if(argc < 3)
    {
        if(argv[argv[1]] != '\n')
        {
            uint32_t data = 0x00;
            uint32_t const addr = (uint32_t)strtoul(&argv[argv[1]], NULL, 16);

            __asm volatile("ldr %0, [%1]" : "+r"(data) : "r"(addr));

            printf("Word read from 0x%x = 0x%x\n", addr, data);
            return;
        }
    }

    printf("ERROR: syntax wrong.\n");
    printf("Usage: w32, write \tADDR DATA\n");

    return;
}

/**
 * @brief Shell reset command
 */
void shell_reset_cmd(char argc, char *argv)
{
    __asm volatile ("b Reset_Handler");
}