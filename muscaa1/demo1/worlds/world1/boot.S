/**
 * SPDX-License-Identifier: Apache-2.0
 * Copyright (c) uTango Project and Contributors. All rights reserved.
 */
    .syntax    unified

.section    .vectors
.align      2
.globl      __Vectors
__Vectors:
    .word   __Stack_Top           /* Top of Stack */
    .word   Reset_Handler         /* Reset Handler */
    .word   NMI_Handler           /* NMI Handler */
    .word   HardFault_Handler     /* Hard Fault Handler */
    .word   MemManage_Handler     /* MPU Fault Handler */
    .word   BusFault_Handler      /* Bus Fault Handler */
    .word   UsageFault_Handler    /* Usage Fault Handler */
    .word   0                     /* Reserved */
    .word   0                     /* Reserved */
    .word   0                     /* Reserved */
    .word   0                     /* Reserved */
    .word   SVC_Handler           /* SVCall Handler */
    .word   DebugMon_Handler      /* Debug Monitor Handler */
    .word   0                     /* Reserved */
    .word   PendSV_Handler        /* PendSV Handler */
    .word   SysTick_Handler       /* SysTick Handler */
    .rept   3
    .word   def_irq_handler
    .endr
    .word   cmsdktimer0_handler

    .size   __Vectors, . - __Vectors

.section    .text
.thumb
.thumb_func
.align      2
.globl      Reset_Handler
.type       Reset_Handler, %function

Reset_Handler:
    ldr     sp, =__Stack_Top
    ldr     r1, =__etext
    ldr     r2, =__data_start__
    ldr     r3, =__data_end__

.L_loop1:
    cmp     r2, r3
    ittt    lt
    ldrlt   r0, [r1], #4
    strlt   r0, [r2], #4
    blt     .L_loop1

    ldr     r1, =__bss_start__
    ldr     r2, =__bss_end__

    movs    r0, 0
.L_loop3:
    cmp     r1, r2
    itt     lt
    strlt   r0, [r1], #4
    blt     .L_loop3

// Call the libc init function
	bl   __libc_init_array

// Call the main function
	bl   main

    /* Should never reach here */
    b      .

    .pool
    .size    Reset_Handler, . - Reset_Handler

.global _init
.type   _init, %function
_init:
    bx lr
    .size  _init, .-_init

.global _fini
.type   _fini, %function
_fini:
    bx lr
    .size  _fini, .-_fini

# -------------------------------------------------------------
.section        .text.cmsdktimer0_handler
.type           cmsdktimer0_handler, %function
.weak           cmsdktimer0_handler               
.thumb_set      cmsdktimer0_handler, def_irq_handler


/*  Macro to define default handlers. */
.macro    def_irq_handler   handler_name
.align    1
.thumb_func
    .weak    \handler_name
    \handler_name:
    b       \handler_name
.endm

def_irq_handler             NMI_Handler
def_irq_handler             HardFault_Handler
def_irq_handler             MemManage_Handler
def_irq_handler             BusFault_Handler
def_irq_handler             UsageFault_Handler
def_irq_handler             SVC_Handler
def_irq_handler             DebugMon_Handler
def_irq_handler             PendSV_Handler
def_irq_handler             SysTick_Handler
def_irq_handler             def_irq_handler
