/**
 * SPDX-License-Identifier: Apache-2.0
 * Copyright (c) uTango Project and Contributors. All rights reserved.
 */
#ifndef __CMSDK_TIMERS_H__
#define __CMSDK_TIMERS_H__

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>

/* ==== CMSDK Timers ======================================================== */
#define CMSDKTIMER0_BASE                        (0x40000000UL)
#define CMSDKTIMER1_BASE                        (0x40001000UL)
#define CMSDKTIMER0                             ((CMSDKTIMER_Type*)(CMSDKTIMER0_BASE))
#define CMSDKTIMER1                             ((CMSDKTIMER_Type*)(CMSDKTIMER1_BASE))

/* struct type to access CMSDK Timer */
typedef struct
{
    volatile uint32_t CTRL;
    volatile uint32_t VALUE;
    volatile uint32_t RELOAD;
    volatile uint32_t INTSTATUSCLEAR;
} CMSDKTIMER_Type;

/* CMSDK Timer masks */
#define CMSDKTIMER0_CTRL_EN                     (1U << 0U)
#define CMSDKTIMER0_CTRL_INTEN                  (1U << 3U)
#define CMSDKTIMER0_INTSTATUSCLEAR_INTCLEAR     (1U << 0U)

#endif /* __CMSDK_TIMERS_H__ */