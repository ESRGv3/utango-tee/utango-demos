/**
 * SPDX-License-Identifier: Apache-2.0
 * Copyright (c) uTango Project and Contributors. All rights reserved.
 */
#ifndef __PWM_IP6512_H__
#define __PWM_IP6512_H__

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>

/* ==== Pulse With Modulation (PWM) - IP6512 (Cadence) ====================== */
#define PWM_BASE                                (0x40107000UL)
#define PWM0                                    ((PWM_Type*)(PWM_BASE + 0x0000))
#define PWM1                                    ((PWM_Type*)(PWM_BASE + 0x7000))
#define PWM2                                    ((PWM_Type*)(PWM_BASE + 0x8000))

/* struct type to access PWM */
typedef struct
{
    volatile uint32_t CR;
    volatile uint32_t PR;
    volatile uint32_t HR;
    volatile uint32_t RESERVED;
    volatile uint32_t EI;
    volatile uint32_t DI;
    volatile uint32_t RI;
    volatile uint32_t IS;
} PWM_Type;

/* PWM masks */
#define PWM_CR_EN                               (1U << 0U)
#define PWM_EI_EN                               (1U << 0U)

#endif /* __PWM_IP6512_H__ */
