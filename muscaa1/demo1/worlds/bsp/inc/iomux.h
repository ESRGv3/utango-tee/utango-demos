/**
 * SPDX-License-Identifier: Apache-2.0
 * Copyright (c) uTango Project and Contributors. All rights reserved.
 */
#ifndef __IOMUX_H__
#define __IOMUX_H__

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>

/* ==== Peripherals Addresses =============================================== */
#define CMSDK_TIMER0_BASE_NS                                (0x40000000UL)
#define CMSDK_TIMER1_BASE_NS                                (0x40001000UL)
#define CMSDK_DUALTIMER_BASE_NS                             (0x40002000UL)
#define MHU0_BASE_NS                                        (0x40003000UL)
#define MHU1_BASE_NS                                        (0x40004000UL)
#define CMSDK_32KTIMER_BASE_NS                              (0x4002F000UL)
#define PWM0_BASE_NS                                        (0x40107000UL)
#define PWM1_BASE_NS                                        (0x4010E000UL)
#define PWM2_BASE_NS                                        (0x4010F000UL)
#define I2S_BASE_NS                                         (0x40106000UL)
#define UART0_BASE_NS                                       (0x40101000UL)
#define UART1_BASE_NS                                       (0x40102000UL)
#define I2C0_BASE_NS                                        (0x40104000UL)
#define I2C1_BASE_NS                                        (0x40105000UL)
#define SPI_BASE_NS                                         (0x40103000UL)
#define GPTIMER_BASE_NS                                     (0x4010B000UL)
#define RTCLOCK_BASE_NS                                     (0x40108000UL)

#ifndef __ASSEMBLER__

#define SCC_BASE                                (0x4010C000UL)
#define SCC                                     ((SCC_Type*)SCC_BASE)

/* struct type to access NSPCB */
typedef struct
{
    volatile uint32_t RESET_CTRL;
    volatile uint32_t CLK_CTRL;
    volatile uint32_t PWR_CTRL;
    volatile uint32_t PLL_CTRL;
    volatile uint32_t DBG_CTRL;
    volatile uint32_t SRAM_CTRL;
    volatile uint32_t INTR_CTRL;
    volatile uint32_t RESERVED1;
    volatile uint32_t CPU0_VTOR_SRAM;
    volatile uint32_t CPU0_VTOR_FLASH;
    volatile uint32_t CPU1_VTOR_SRAM;
    volatile uint32_t CPU1_VTOR_FLASH;
    volatile uint32_t IOMUX_MAIN_INSEL;
    volatile uint32_t IOMUX_MAIN_OUTSEL;
    volatile uint32_t IOMUX_MAIN_OENSEL;
    volatile uint32_t IOMUX_MAIN_DEFAULT_IN;
    volatile uint32_t IOMUX_ALTF1_INSEL;
    volatile uint32_t IOMUX_ALTF1_OUTSEL;
    volatile uint32_t IOMUX_ALTF1_OENSEL;
    volatile uint32_t IOMUX_ALTF1_DEFAULT_IN;
    volatile uint32_t IOMUX_ALTF2_INSEL;
    volatile uint32_t IOMUX_ALTF2_OUTSEL;
    volatile uint32_t IOMUX_ALTF2_OENSEL;
    volatile uint32_t IOMUX_ALTF2_DEFAULT_IN;
    volatile uint32_t PVT_CTRL;
    volatile uint32_t SPARE0;
    volatile uint32_t IOPAD_DS0;
    volatile uint32_t IOPAD_DS1;
    volatile uint32_t IOPAD_PE;
    volatile uint32_t IOPAD_PS;
    volatile uint32_t IOPAD_SR;
    volatile uint32_t IOPAD_IS;
    volatile uint32_t SRAM_RW_MARGINE;
    volatile uint32_t STATIC_CONF_SIG0;
    volatile uint32_t STATIC_CONF_SIG1;
    volatile uint32_t REQ_SET;
    volatile uint32_t REQ_CLEAR;
    volatile uint32_t IOMUX_ALTF3_INSEL;
    volatile uint32_t IOMUX_ALTF3_OUTSEL;
    volatile uint32_t IOMUX_ALTF3_OENSEL;
    volatile uint32_t IOMUX_ALTF3_DEFAULT_IN;
    volatile uint32_t PCSM_CTRL_OVERRIDE;
    volatile uint32_t PD_CPU0_ISO_OVERRIDE;
    volatile uint32_t PD_CPU1_ISO_OVERRIDE;
    volatile uint32_t SYS_SRAM_RW_ASSIST0;
    volatile uint32_t SYS_SRAM_RW_ASSIST1;
    volatile uint32_t SYS_SRAM_RW_ASSIST2;
    volatile uint32_t SYS_SRAM_RW_ASSIST3;
    volatile uint32_t SYS_SRAM_RW_ASSIST4;
    volatile uint32_t SYS_SRAM_RW_ASSIST5;
    volatile uint32_t RESERVED2[3];
    volatile uint32_t CRYPTO_SRAM_RW_ASSIST0;
    volatile uint32_t CRYPTO_SRAM_RW_ASSIST1;
    volatile uint32_t CRYPTO_SRAM_RW_ASSIST2;
    volatile uint32_t REQ_EDGE_SEL;
    volatile uint32_t REQ_ENABLE;
    volatile uint32_t RESERVED3[28];
    volatile uint32_t CHIP_ID;
    volatile uint32_t CLOCK_STATUS;
    volatile uint32_t IO_IN_STATUS;
} SCC_Type;

typedef enum gpio_func
{
    GPIO_MAINF = 0UL,
    GPIO_ALTF1,
    GPIO_ALTF2,
    GPIO_ALTF3,
}gpio_func_t;

typedef enum gpio_pin
{
    PA0 = 0U,
    PA1,
    PA2,
    PA3,
    PA4,
    PA5,
    PA6,
    PA7,
    PA8,
    PA9,
    PA10,
    PA11,
    PA12,
    PA13,
    PA14,
    PA15,
}gpio_pin_t;

typedef struct pin_desc
{
    gpio_func_t func;
    gpio_pin_t pin;
}pin_desc_t;

typedef struct iomux_dev_cfg
{
    uint32_t dev_addr;
    uint8_t n_pins;
    pin_desc_t *pin_cfg;
}iomux_dev_cfg_t;

void iomux_config(uint32_t dev_addr);

#endif  /* |__ASSEMBLER__ */
#endif /* __IOMUX_H__ */
