/**
 * SPDX-License-Identifier: Apache-2.0
 * Copyright (c) uTango Project and Contributors. All rights reserved.
 */

#include <iomux.h>

iomux_dev_cfg_t iomuxtree [] =
{
    /* UART0 */
    {
        .dev_addr = UART0_BASE_NS,
        .n_pins = 2,
        .pin_cfg = (pin_desc_t[])
        {
            {   //UART0 RxD
                .func = GPIO_ALTF1,
                .pin = PA0,
            },
            {   //UART0 TxD
                .func = GPIO_ALTF1,
                .pin = PA1,
            }
        },
    },
    /* I2S */
    {
        .dev_addr = I2S_BASE_NS,
        .n_pins = 8,
        .pin_cfg = (pin_desc_t[])
        {
            {   //MR_I2S_SD
                .func = GPIO_ALTF1,
                .pin = PA2,
            },
            {   //MR_I2S_WS
                .func = GPIO_ALTF1,
                .pin = PA3,
            },
            {   //MR_I2S_SCK
                .func = GPIO_ALTF1,
                .pin = PA4,
            },
            {   //MR_I2S_SD0
                .func = GPIO_ALTF1,
                .pin = PA5,
            },
            {   //MR_I2S_WSO
                .func = GPIO_ALTF1,
                .pin = PA6,
            },
            {   //MR_I2S_SD1
                .func = GPIO_ALTF1,
                .pin = PA7,
            },
            {   //MR_I2S1_WS1
                .func = GPIO_ALTF1,
                .pin = PA8,
            },
            {   //MR_I2S_SCK
                .func = GPIO_ALTF1,
                .pin = PA9,
            }
        },
    },
    /* SPI0 */
    {
        .dev_addr = SPI_BASE_NS,
        .n_pins = 4,
        .pin_cfg = (pin_desc_t[])
        {
            {   //SPI0 nSS0
                .func = GPIO_ALTF1,
                .pin = PA10,
            },
            {   //SPI0 MOSI
                .func = GPIO_ALTF1,
                .pin = PA11,
            },
            {   //SPI0 MISO
                .func = GPIO_ALTF1,
                .pin = PA12,
            },
            {   //SPI0 SCK
                .func = GPIO_ALTF1,
                .pin = PA13,
            }
        },
    },
    /* I2C0 */
    {
        .dev_addr = I2C0_BASE_NS,
        .n_pins = 2,
        .pin_cfg = (pin_desc_t[])
        {
            {   //I2C0 Data
                .func = GPIO_ALTF1,
                .pin = PA14,
            },
            {   //I2C0 Clock
                .func = GPIO_ALTF1,
                .pin = PA15,
            }
        },
    },
    /* PWM0 */
    {
        .dev_addr = PWM0_BASE_NS,
        .n_pins = 1,
        .pin_cfg = (pin_desc_t[])
        {
            {   //PWM0
                .func = GPIO_ALTF2,
                .pin = PA2,
            }
        },
    },
    /* PWM1 */
    {
        .dev_addr = PWM1_BASE_NS,
        .n_pins = 1,
        .pin_cfg = (pin_desc_t[])
        {
            {   //PWM1
                .func = GPIO_ALTF2,
                .pin = PA3,
            }
        },
    },
    /* PWM2 */
    {
        .dev_addr = PWM2_BASE_NS,
        .n_pins = 1,
        .pin_cfg = (pin_desc_t[])
        {
            {   //PWM2
                .func = GPIO_ALTF2,
                .pin = PA4,
            }
        },
    },
};

void iomux_clear_altfunc(bool main_func, bool altfunc1, bool altfunc2, uint32_t pinmask)
{
    SCC_Type *scc_ptr = NULL;
    
    scc_ptr = (SCC_Type*)SCC_BASE;

    if(main_func) 
    {
        scc_ptr->IOMUX_MAIN_INSEL &= (~pinmask);
        scc_ptr->IOMUX_MAIN_OUTSEL &= (~pinmask);
        scc_ptr->IOMUX_MAIN_OENSEL &= (~pinmask);
    }
    if(altfunc1) 
    {
        scc_ptr->IOMUX_ALTF1_INSEL &= (~pinmask);
        scc_ptr->IOMUX_ALTF1_OUTSEL &= (~pinmask);
        scc_ptr->IOMUX_ALTF1_OENSEL &= (~pinmask);
    }
    if(altfunc2) 
    {
        scc_ptr->IOMUX_ALTF2_INSEL &= (~pinmask);
        scc_ptr->IOMUX_ALTF2_OUTSEL &= (~pinmask);
        scc_ptr->IOMUX_ALTF2_OENSEL &= (~pinmask);
    }
}

void iomux_set_altfunc(uint8_t dev)
{
    SCC_Type *scc_ptr = NULL;
    
    scc_ptr = (SCC_Type*)SCC_BASE;

    volatile uint32_t *iomux_insel = NULL;
    volatile uint32_t *iomux_outsel = NULL;
    volatile uint32_t *iomux_oensel = NULL;

    for (int i = 0; i < iomuxtree[dev].n_pins; i++)
    {
        uint32_t pin_mask = (1 << iomuxtree[dev].pin_cfg[i].pin);
        gpio_func_t pin_func = iomuxtree[dev].pin_cfg[i].func;

        switch (pin_func)
        {
            case GPIO_MAINF:
                iomux_insel = &scc_ptr->IOMUX_MAIN_INSEL;
                iomux_outsel = &scc_ptr->IOMUX_MAIN_OUTSEL;
                iomux_oensel = &scc_ptr->IOMUX_MAIN_OENSEL;
                break;
            case GPIO_ALTF1:
                iomux_insel = &scc_ptr->IOMUX_ALTF1_INSEL;
                iomux_outsel = &scc_ptr->IOMUX_ALTF1_OUTSEL;
                iomux_oensel = &scc_ptr->IOMUX_ALTF1_OENSEL;
                break;
            case GPIO_ALTF2:
                iomux_insel = &scc_ptr->IOMUX_ALTF2_INSEL;
                iomux_outsel = &scc_ptr->IOMUX_ALTF2_OUTSEL;
                iomux_oensel = &scc_ptr->IOMUX_ALTF2_OENSEL;
                break;
            case GPIO_ALTF3:
                /* Reserved */
                break;
        default:
            break;
        }

        /* Select the wanted function's output enable bit */
        *iomux_oensel |= pin_mask;

        /* Clear all alternate function */
        if(pin_func == GPIO_MAINF)
        {iomux_clear_altfunc(false, true, true, pin_mask);}
        else if(pin_func == GPIO_ALTF1)
        {iomux_clear_altfunc(true, false, true, pin_mask);}
        else if(pin_func == GPIO_ALTF2)
        {iomux_clear_altfunc(true, true, false, pin_mask);}

        /* Enable input and output data line */
        *iomux_insel  |= pin_mask;
        *iomux_outsel |= pin_mask;
    }
}

void iomux_config(uint32_t dev_addr)
{
    for(int i = 0; i < (sizeof(iomuxtree)/sizeof(iomux_dev_cfg_t)); i++)
    {
        if(dev_addr == iomuxtree[i].dev_addr)
        {
            iomux_set_altfunc(i);
        }
    }
}