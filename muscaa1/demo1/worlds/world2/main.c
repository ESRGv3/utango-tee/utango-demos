/**
 * SPDX-License-Identifier: Apache-2.0
 * Copyright (c) uTango Project and Contributors. All rights reserved.
 */
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include "uart_pl011.h"
#include "nr_micro_shell.h"
#include "shell_cmd.h"

#define MUSCAB1_UART0_NS_BASE                     (0x40102000UL)

/* Define UART PL011 device structs */
static const struct uart_pl011_dev_cfg_t UART0_PL011_DEV_CFG_NS = 
{
    .base = MUSCAB1_UART0_NS_BASE,
    .def_baudrate = DEFAULT_UART_BAUDRATE,
    .def_wlen = UART_PL011_WLEN_8,
    .def_parity = UART_PL011_PARITY_DISABLED,
    .def_stopbit = UART_PL011_STOPBIT_1
};
static struct uart_pl011_dev_data_t UART0_PL011_DEV_DATA_NS = 
{
    .state = UART_PL011_UNINITIALIZED,
    .uart_clk = 0,
    .baudrate = 0
};
struct uart_pl011_dev_t UART0_PL011_DEV_NS = 
{
    &(UART0_PL011_DEV_CFG_NS),
    &(UART0_PL011_DEV_DATA_NS)
};

const static_cmd_st static_cmd[] =
{
    {"rst", shell_reset_cmd},
    {"reset", shell_reset_cmd},
    {"r32", shell_read_cmd},
    {"read", shell_read_cmd},
    {"w32", shell_write_cmd},
    {"write", shell_write_cmd},
    {"help", shell_help_cmd},
    {"\0", NULL}
};

bool uart_init(void)
{
    //115200, 8 bits, no parity, 1 stop bit
    uart_pl011_init(&UART0_PL011_DEV_NS, CLOCK);
    uart_pl011_enable(&UART0_PL011_DEV_NS);

    if (uart_pl011_is_writable(&UART0_PL011_DEV_NS))
        return true;

    return false;
}

int main(void)
{
    char c;

    /* Init UART device */
    uart_init();

    /* Init shell command line interface */
    shell_init();

    while(1)
    {
        c = getchar();
        shell(c);
    }

    return 0;
}
