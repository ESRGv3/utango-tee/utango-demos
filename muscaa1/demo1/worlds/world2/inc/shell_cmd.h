/**
 * SPDX-License-Identifier: Apache-2.0
 * Copyright (c) uTango Project and Contributors. All rights reserved.
 */
#ifndef __SHELL_CMD_H__
#define __SHELL_CMD_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/**
 * @brief Shell commands declaration
 */
void shell_help_cmd(char argc, char *argv);
void shell_write_cmd(char argc, char *argv);
void shell_read_cmd(char argc, char *argv);
void shell_reset_cmd(char argc, char *argv);

#endif /* __SHELL_CMD_H__ */