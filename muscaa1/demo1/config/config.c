/**
 * SPDX-License-Identifier: Apache-2.0
 * Copyright (c) uTango Project and Contributors. All rights reserved.
 */

#include <utango.h>

#ifndef UTANGO_SIZE
/**
 * Declare WORLDS images using the WORLD_IMAGE macro, passing an identifier and 
 * the path for the image.
 */
WORLD_IMAGE(world1, ../../../worlds/world1/blinking_led.bin);
WORLD_IMAGE(world2, ../../../worlds/world2/cmdline.bin);
#endif

/**
 * Config each WORLD.
 */
world_cfg_t  worlds_cfg[UTANGO_WORLDS] =
{
    /* World 1 (Blinking Red LED) */
    {
        .id = 0,
        .vect_base_addr = 0x00000000,
        .init_sp = 0x2000a000,
        .entry_point = 0x00000051,
        .n_regions = 5,
        .w_regions = (world_regions_t[])
        {
            {   //FLASH (Code_SRAM0)
                .rgn_base_addr = 0x00000000,
                .rgn_size = 0x4000
            },
            {   //RAM (iSRAM0)*/
                .rgn_base_addr = 0x20008000,
                .rgn_size = 0x2000
            },
            {   //PWM0
                .rgn_base_addr = 0x40107000,
                .rgn_size = 0x100
            },
            {   //CMSDK Timer0
                .rgn_base_addr = 0x40000000,
                .rgn_size = 0x100
            },
            {   //SCC
                .rgn_base_addr = 0x4010C000,
                .rgn_size = 0x100
            }
        },
        .n_irqs = 1,
        .enabled_irqs = (irqs_list_t[])
        {CMSDK_TIMER0_IRQn},
    },
    /* World 2 (Serial Cmdline) */
    {
        .id = 1,
        .vect_base_addr = 0x00080000,
        .init_sp = 0x20012000,
        .entry_point = 0x00080145,
        .n_regions = 3,
        .w_regions = (world_regions_t[])
        {
            {   //FLASH (Code_SRAM1)
                .rgn_base_addr = 0x00080000,
                .rgn_size = 0x4000
            },
            {   //RAM (iSRAM1)
                .rgn_base_addr = 0x20010000,
                .rgn_size = 0x2000
            },
            {   //UART0
                .rgn_base_addr = 0x40102000,
                .rgn_size = 0x1000
            }
        },
        .n_irqs = 0,
        .enabled_irqs = (irqs_list_t[])
        {},
    },
};
