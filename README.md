# uTango Demos

uTango is a multi-World TEE for resource-constrained IoT Devices. In this repo
you will find several demos ready-to-run for each of the supported platforms.

## Supported boards
- [x] ARM MUSCA-A1
- [x] ARM MUSCA-B1
- [ ] STM NUCLEO-L552ZE-Q

## Step 0) Prerequisites

Install the following dependencies essential to clone and build uTango.

`sudo apt install build-essential git`

Install the pyOCD debugger/programmer that will be used to program the
supported boards.

`pip install pyocd==0.34.3`

Download the following bare-metal cross-compile toolchain from the [Arm's website](https://developer.arm.com/-/media/Files/downloads/gnu-rm/10-2020q4/gcc-arm-none-eabi-10-2020-q4-major-x86_64-linux.tar.bz2?revision=ca0cbf9c-9de2-491c-ac48-898b5bbc0443&rev=ca0cbf9c9de2491cac48898b5bbc0443&hash=0C735F2481B3EDAB54EF4E68321CE01F). Install the toolchain and set the **CROSS_COMPILE** environment variable.

`export CROSS_COMPILE=<toolchain-path-root>/bin/arm-none-eabi-`

## Step 1) Download uTango
First, clone the uTango kernel into your filesystem.

`git clone https://gitlab.com/danieljcoliveira/utango.git`

## Step 2) Download the demo
Clone this repo into your filesystem.

`git clone https://gitlab.com/danieljcoliveira/utango-demos.git`

Now copy the provided uTango configuration and worlds into uTango's tree.
Substitute the `<board>` by your target and `<demo>` by the demo version (Check
[here](#demos-overview) the available demos).

`cp -r utango-demos/<board>/<demo>/config utango/src/`\
`cp -r utango-demos/<board>/<demo>/worlds utango/`

## Step 3) Compile uTango and each world application

Once again, don't forget to substitute the `<board>` by your target.

`cd utango`\
`make worlds`\
`make BOARD=<board>`

## Step 3) Load uTango to your target

`make BOARD=<board> load`

## Step 4) Connect to a serial interface

`sudo minicom -s -c on -D <device-node>`

Device nodes: `/dev/ttyACM0` , `/dev/ttyUSB0`

## Demos overview

### Description

| Demo  | Worlds | Description                                                                   |
|-------|--------|-------------------------------------------------------------------------------|
| demo1 | World1 | Bare-metal application blinking an LED each 1s by using a timer interrupt     |
|       | World2 | Bare-metal application providing a command line with several testing commands |

### Board setup

| Board       | Demo     | Worlds | Devices     |
|-------------|----------|--------|-------------|
| **muscaa1** | demo1    | World1 | TIMER0/PWM0 |
|             |          | World2 | UART0       |
| **muscab1** | demo1    | World1 | TIMER0/PWM0 |
|             |          | World2 | UART1       |

## Appendix I

| Tool              | Version |
|------------------ |---------|
| arm-none-eabi-gcc | 10.2.1  |
| make              | 4.2.1   |
| pyocd             | 0.34.3  |
| minicom           | 2.7.1   |
